pregunta = "Escribi un numero y te digo si es par o no \r\n"
pregunta += "Escribi 'Cerrar' para salir de la aplicacion \r\n"
preguntar = True

while preguntar:
    numero = input(pregunta)

    if numero == "Cerrar": 
        preguntar = False
    else:
        numero = int(numero)
        if numero % 2 == 0:
            print(f"El numero {numero} ingresado es PAR")
        else:
            print(f"El numero {numero} ingresado es IMPAR")
