playlist = {} # Dicc vacio
playlist["Canciones"] = [] # Lista vacia de canciones

# Funcion principal
def app():
    # Agregar playlist
    agregar_playlist = True
    while agregar_playlist:
        nombre_playlist = input("¿Como deseas nombrar la playlist?\r\n")
        if nombre_playlist:
            playlist["Nombre"] = nombre_playlist
            
            # Como ya tenemos el nombre se desactiva el True
            agregar_playlist = False

            # Mandar a llamar la funcion para agregar canciones
            agregar_canciones()

def agregar_canciones():
    agregar_cancion = True
    while agregar_cancion:
        # Preguntar al usuario que cancion quiere agregar
        nombre_playlist = playlist["Nombre"]

        pregunta = f"\r\nAgrega canciones para la playlist {nombre_playlist} \r\n"
        pregunta += "Escribi 'X' para dejar de agregar canciones \r\n"

        cancion = input(pregunta)
        if cancion == "X":
            # Dejo de agregar canciones
            agregar_cancion = False
            # Mostrar resumen
            mostrar_resumen()
        else:
            # Agrego canciones
            playlist["Canciones"].append(cancion)

def mostrar_resumen():
    nombre_playlist = playlist["Nombre"]
    print(f"\r\nPlaylist: {nombre_playlist}\r\n")
    print("Canciones:")
    for cancion in playlist["Canciones"]:
        print(cancion)

app()