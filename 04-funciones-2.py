# Si quiero poner Strings en los parametros tengo que poner una "f" al principio
def informacion(nombre, puesto = "estudiante"): 
    print(f"Soy {nombre} y soy {puesto}")

informacion("Nachito", "programador") 
informacion("Tomas", "estudiante") 
informacion("Guido", "ingeniero") 
informacion("Nico") 
