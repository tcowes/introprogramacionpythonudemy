# Iniciar un diccionario vacio

jugador = {}
print(jugador)

# Se une un jugador
jugador["Nombre"] = "Tomás"
jugador["Puntaje"] = 0
print(jugador)

# Incrementando el puntaje
jugador["Puntaje"] = 100
print(jugador)

# Acceder a un valor
print(jugador.get("Nombre"))

# Iterar en el diccionario
for llave, valor in jugador.items():
    print(llave)
    print(valor)

# Eliminar jugador y puntaje
del jugador["Nombre"]
del jugador["Puntaje"]
print(jugador)