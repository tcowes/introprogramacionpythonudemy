def bienvenida():
    print("¡Bienvenido a Python!")

def bienvenida_nombre(nombre):
    print(f"¡Bienvenido {nombre} a Python!")

bienvenida()
bienvenida_nombre("Tomás")