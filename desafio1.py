jugador = input("¿Cual es tu nombre? \r\n")
puntaje = 0

print(f"Bienvenido al desafio {jugador}, el reto consiste en responder 3 preguntas con SI o NO (responder en mayuscula) y al final se te da una calificacion, mucha suerte!")

pregunta_1 = input("¿Londres es la capital de Inglaterra? \r\n")
if pregunta_1 == "SI":
    puntaje += 1

pregunta_2 = input("¿Barcelona es la capital de España? \r\n")
if pregunta_2 == "NO":
    puntaje += 1

pregunta_3 = input("¿El Reino Unido pertenece a la Union Europea? \r\n")
if pregunta_3 == "NO":
    puntaje += 1

print(f"Muchas gracias por responder {jugador}!, tu puntaje final es: {puntaje}")