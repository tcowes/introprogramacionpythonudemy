def app():
    # Crear un archivo
    archivo = open("archivo.txt", "w") # W es permiso de escritura, de no existir lo crea

    # Generar numeros en archivo
    for i in range(0, 20):
        archivo.write("Esta es la linea: " + str(i) + "\r\n")
    
    # Cerrar el archivo
    archivo.close()
    
app()