class Restaurant:

    def __init__(self, nombre, categoria, precio):
        self.nombre = nombre
        self.categoria = categoria
        self.__precio = precio 

    def mostrar_informacion(self):
        print(f"Nombre: {self.nombre}, Categoria: {self.categoria}, Precio: {self.__precio}")
    
    def get_precio(self):
        return self.__precio
    
    def set_precio(self, nuevo_precio):
        self.__precio = nuevo_precio

# Crear una clase hijo de Restaurant
class Hotel(Restaurant):

    def __init__(self, nombre, categoria, precio):
        super().__init__(nombre, categoria, precio)

hotel = Hotel("Hosteria del Lago", "4 Estrellas", "200 dolares")
hotel.mostrar_informacion()