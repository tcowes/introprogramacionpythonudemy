class Restaurant:

    def agregar_restaurant(self, nombre):
        self.nombre = nombre # Atributo
    
    def mostrar_informacion(self):
        print(f"Nombre: {self.nombre}")

# Instanciar la clase
restaurant = Restaurant()
restaurant.agregar_restaurant("Pizzeria Argentina")
restaurant.mostrar_informacion()

restaurant2 = Restaurant()
restaurant2.agregar_restaurant("Los tres Ases")
restaurant2.mostrar_informacion()

# Mostrar informacion
print(f"Nombre Restaurant: {restaurant.nombre}")
print(f"Nombre Restaurant: {restaurant2.nombre}")