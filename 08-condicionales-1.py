# Revisar si una condicion es mayor a...
balance = 0
if balance > 0:
    print("El usuario puede pagar")
else:
    print("El usuario no tiene suficiente saldo")


# Likes
likes = 200
if likes == 200:
    print("Excelente, tenés 200 likes")

# Ahora con texto y el NOT
lenguaje = "PHP"
if not lenguaje == "Python":
    print("Gran decision")

# Booleanos
aprobado = True
if aprobado:
    print("Muy bien, aprobaste!")
else:
    print("Ojala la proxima te vaya mucho mejor!")

# Evaluar elementos de una lista
lenguajes = ["Python", "Kotlin", "Java", "JavaScript", "PHP", "Gobstones", "C++"]
if "Java" in lenguajes:
    print("Java si existe")
else:
    print("Java no esta en la lista")

# If anidados (No tan lindo...)
usuario_autenticado = True
usuario_admin = False

if usuario_autenticado:
    if usuario_admin:
        print("Tenes acceso total")
    else:
        print("Tenes acceso parcial a la plataforma")
else:
    print("Usuario no autenticado, tenes que loguearte")