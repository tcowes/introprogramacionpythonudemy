class Restaurant:

    def __init__(self, nombre, categoria, precio):
        self.nombre = nombre
        self.categoria = categoria
        self.precio = precio 

    def mostrar_informacion(self):
        print(f"Nombre: {self.nombre}, Categoria: {self.categoria}, Precio: {self.precio}")
    
    def get_precio(self):
        return self.precio
    
    def set_precio(self, nuevo_precio):
        self.precio = nuevo_precio

# Crear una clase hijo de Restaurant
class Hotel(Restaurant):

    def __init__(self, nombre, categoria, precio, alberca):
        super().__init__(nombre, categoria, precio) # Esto lo hereda del padre
        self.alberca = alberca # Esto es especifico de esta clase

    # Reescribir un metodo
    def mostrar_informacion(self):
        print(f"Nombre: {self.nombre}, Categoria: {self.categoria}, Precio: {self.precio}, Alberca: {self.alberca}")

hotel = Hotel("Hosteria del Lago", "4 Estrellas", "200 dolares", "Si")
hotel.mostrar_informacion()