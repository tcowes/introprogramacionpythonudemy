lenguajes = ["Python", "Kotlin", "Java", "JavaScript"]

print(lenguajes)
print(lenguajes[2]) # Los List comienzan en la posicion 0

# Ordenar los elementos
lenguajes.sort()
print(lenguajes)

# Acceder a un elemento dentro de un texto
aprendiendo = f"Estoy aprendiento {lenguajes[3]}"
print(aprendiendo)

# Sustituir elementos, modificando valores del array
lenguajes[2] = "PHP"
print(lenguajes)

# Agregar elemenos al array
lenguajes.append("Ruby")
print(lenguajes)

# Eliminar elementos
del lenguajes[1]
print(lenguajes)

# Eliminar el ultimo elemento
lenguajes.pop() # Si pongo un parametro puedo eliminar elementos en un orden especifico
print(lenguajes)

# Eliminar elementos.3
lenguajes.remove("PHP")
print(lenguajes)