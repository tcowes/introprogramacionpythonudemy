nombre = input("¿Cual es tu nombre? \r\n")

print(f"Hola {nombre}")

# Leer datos que sean numeros
edad = input("¿Cual es tu edad? \r\n")
# Convertir edad ingresada en String a Entero
edad = int(edad)

if edad >= 18:
    print("Ya podes votar")
else:
    print("Aun no podés votar")

# Mezclar con operadores
numero = input("Escribi un numero y te digo si es par o no \r\n")
numero = int(numero)

if numero % 2 == 0: # El % te deja como el resultado el residual de la division
    print(f"El numero {numero} ingresado es PAR")
else:
    print(f"El numero {numero} ingresado es IMPAR")