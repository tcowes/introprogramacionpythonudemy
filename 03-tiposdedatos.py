# Los strings se escriben con doble comilla " o comilla simple '
nombre = "Tomas Cowes"
print(nombre)

# Enteros
edad = 22
print(edad)

# Floats
cantidad_a_pagar = 100.20
print(cantidad_a_pagar)

# Bool
pagado = True
print(pagado) 