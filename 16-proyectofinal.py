import os

CARPETA = "contactos/" # Carpeta de contactos, se escribe en mayusc porque es CONST
EXTENSION = ".txt" # Extension del archivo

# Contactos
class Contacto:
    def __init__(self, nombre, telefono, categoria):
        self.nombre = nombre
        self.telefono = telefono
        self.categoria = categoria

def app():
    # Revisa si la carpeta existe o no
    crear_directorio()
    # Muestra el menu de opciones
    mostrar_menu()
    # Preguntar la accion a realizar
    preguntar = True
    while preguntar:
        opcion = input("Seleccioná una opción:\r\n")
        opcion = int(opcion)

        # Ejecutar las opciones (no es mejor hacer una subtarea?)
        if opcion == 1:
            agregar_contacto()
        elif opcion == 2:
            editar_contacto()
        elif opcion == 3:
            mostrar_contactos()
        elif opcion == 4:
            buscar_contacto()
        elif opcion == 5:
            eliminar_contacto()
        elif opcion == 6:
            preguntar = False
        else: 
            print("No seleccionaste una opcion válida")

def editar_contacto():
    nombre_anterior = input("Nombre del contacto que querés editar: \r\n")
    existe = existe_contacto(nombre_anterior)

    if existe:
        with open(CARPETA + nombre_anterior + EXTENSION, "w") as archivo:
            nombre_contacto = input("Modificá el nombre:\r\n")
            telefono_contacto = input("Modificá el Teléfono:\r\n")
            categoria_contacto = input("Modificá la categoria:\r\n")

            contacto = Contacto(nombre_contacto, telefono_contacto, categoria_contacto)

            archivo.write("Nombre: " + contacto.nombre + "\r\n")
            archivo.write("Teléfono: " + contacto.telefono + "\r\n")
            archivo.write("Categoria: " + contacto.categoria + "\r\n")
        # Renombrar el archivo
        os.rename(CARPETA + nombre_anterior + EXTENSION, CARPETA + nombre_contacto + EXTENSION)
        print("\r\n ¡Contacto editado correctamente! \r\n")
    else: 
        print("ERROR: ¡Este contacto no existe!")
    app()

def agregar_contacto():
    print("Escribí los datos para agregar el nuevo Contacto")
    nombre_contacto = input("Nombre del Contacto:\r\n")

    # Revisar si el archivo ya existe antes de crearlo
    existe = existe_contacto(nombre_contacto)

    if not existe:
        with open(CARPETA + nombre_contacto + EXTENSION, "w") as archivo:
            
            # Resto de los campos
            telefono_contacto = input("Agregá el Teléfono:\r\n")
            categoria_contacto = input("Categoria del Contacto:\r\n")

            # Instanciar la clase
            contacto = Contacto(nombre_contacto, telefono_contacto, categoria_contacto)

            archivo.write("Nombre: " + contacto.nombre + "\r\n")
            archivo.write("Teléfono: " + contacto.telefono + "\r\n")
            archivo.write("Categoria: " + contacto.categoria + "\r\n")

            # Mostar un mensaje exitoso
            print("\r\n ¡Contacto creado correctamente! \r\n")
    else:
        print("ERROR: ¡Este contacto ya existe!")
    app() # Con esto vuelvo a reiniciar la app

def mostrar_contactos():
    archivos = os.listdir(CARPETA)
    
    # Validar que solo se muestren archivos que terminen en la extension .txt
    archivos_txt = [i for i in archivos if i.endswith(EXTENSION)]

    for archivo in archivos_txt:
        with open(CARPETA + archivo) as contacto:
            for linea in contacto:
                # Imprime el contenido
                print(linea.rstrip())
            # Imprime la separacion entre cada uno    
            print("\r\n") 
    app()

def buscar_contacto():
    nombre = input("Seleccioná el Contacto que querés buscar:\r\n")

    try:
        with open(CARPETA + nombre + EXTENSION) as contacto:
            print("\r\n Información del Contacto: \r\n")
            for linea in contacto: 
                print(linea.rstrip())
            print("\r\n")
    except IOError:
        print("El archivo no existe")
        print(IOError)
    app()

def eliminar_contacto():
    contacto_a_eliminar = input("¿Que contacto querés eliminar? \r\n")

    try:
        os.remove(CARPETA + contacto_a_eliminar + EXTENSION)
        print("¡Contacto eliminado!")
    except:
        print("ERROR: ¡Este contacto no existe!")
    app()

def mostrar_menu():
    print("Seleccioná en este Menú que es lo que querés hacer:")
    print("1 - Agregar Nuevo Contacto")
    print("2 - Editar Contacto")
    print("3 - Ver Contactos")
    print("4 - Buscar Contacto")
    print("5 - Eliminar Contacto")
    print("6 - Salir")

def crear_directorio():
    if not os.path.exists("contactos/"):
        # crear la carpeta
        os.makedirs("contactos/")

def existe_contacto(nombre):
    return os.path.isfile(CARPETA + nombre + EXTENSION)

app()