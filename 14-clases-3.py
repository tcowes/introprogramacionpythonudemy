class Restaurant:

    def __init__(self, nombre, categoria, precio):
        self.nombre = nombre # Atritbuto por default: Publico
        self.categoria = categoria
        self.__precio = precio # Con el _ se vuelve PROTECTED y __ PRIVATE
        # Siendo PRIVATE solo se puede consultar mediante un getter

    def mostrar_informacion(self):
        print(f"Nombre: {self.nombre}, Categoria: {self.categoria}, Precio: {self.__precio}")
    
    # GETTERS Y SETTERS:
    def get_precio(self):
        return self.__precio
    
    def set_precio(self, nuevo_precio):
        self.__precio = nuevo_precio

# Instanciar la clase
restaurant = Restaurant("Pizzeria Argentina", "Pizza", "50 dolares")

precio1 = restaurant.get_precio()
print(precio1)
restaurant.set_precio(40)
restaurant.mostrar_informacion()

restaurant2 = Restaurant("Los tres Ases", "Pizza", "70 dolares")

precio2 = restaurant2.get_precio()
print(precio2)
restaurant2.set_precio(100)
restaurant2.mostrar_informacion()
