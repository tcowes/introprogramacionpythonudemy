numero = 0

# while numero <= 10:
#    print(numero)
#    numero += 1 # Si no incremento entro en un loop infinito

# IF DENTRO DE UN WHILE
while numero <= 10:
    if numero == 5:
        break
    else:
        print(numero)
        numero += 1