# Creando un diccionario simple

cancion = {
    "Artista" : "Metallica", # Llave y valor
    "Cancion" : "Enter Sandman", 
    "Lanzamiento" : 1992,
    "Likes" : 3000
}

artista = cancion["Artista"] # Si lo asigno en una variable es mas facil de llamar y no tira error

print(f"Estoy escuchando {artista}")

# Agregar un nuevo valor
cancion["Genero"] = "Heavy Metal"
print(cancion)

# Reemplazar un valor existente
cancion["Cancion"] = "Seek & Destroy"
print(cancion)

# Eliminar un valor
del cancion["Lanzamiento"]
print(cancion)