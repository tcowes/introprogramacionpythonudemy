class Restaurant:

    def __init__(self, nombre, categoria, precio):
        self.nombre = nombre
        self.categoria = categoria
        self.precio = precio

    def mostrar_informacion(self):
        print(f"Nombre: {self.nombre}, Categoria: {self.categoria}, Precio: {self.precio}")

# Instanciar la clase
restaurant = Restaurant("Pizzeria Argentina", "Pizza", "50 dolares")
restaurant.mostrar_informacion()

restaurant2 = Restaurant("Los tres Ases", "Pizza", "70 dolares")
restaurant2.mostrar_informacion()