def suma(a = 0, b = 0):
    print(a + b)

suma(35,1)
suma(10,-8)

def resta(a, b):
    print(a - b)

resta(5,3)
resta(109,9.1)

def multiplicacion(a, b):
    print(a * b)

multiplicacion(2,2)
multiplicacion(1,0)

def division(a, b):
    print(a / b)

division(4,2)
division(10,5)